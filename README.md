# Developer activity

**Developer activity** fetches local users activities on Drupal.org and on GitHub.com into custom entities. It uses public endpoints and runs at each cron, so all you need are a Drupal.org user ID and a GitHub.com user name fields for your Drupal users. You can then create Views with those custom entities, for instance.

The used feeds are (examples with my accounts):
+ Nodes created on Drupal.org: https://www.drupal.org/api-d7/node.json?author=173213&sort=nid&direction=DESC
+ Comments created on Drupal.org: https://www.drupal.org/api-d7/comment.json?author=173213&sort=nid&direction=DESC
+ GitHub user Atom feed: https://github.com/guix77.atom

Note: there is no paginated fetching, which means that only the items immediatly retrieved are fetched. This can lead to having old Drupal nodes next to a lot more recent Drupal.org comments / GitHub activities. That's why I suggest to order the results by the *Published at* field in *DESC* order when creating a View.

A demonstration can be seen on my [Drupal developer](https://guillaumeduveau.com/en#block-views-block-developers-activity-block-1) site.

## Dependencies

+ [Migrate plus](https://www.drupal.org/project/migrate_plus) 8.x-5.x
+ Your Drupal site must have a [working cron](https://www.drupal.org/docs/8/administering-a-drupal-8-site/cron-automated-tasks/cron-automated-tasks-overview#s-enabling-cron)

## Installation

If you use composer: ````composer require drupal/developer_activity````

Otherwise please install [Migrate plus](https://www.drupal.org/project/migrate_plus) 8.x-5.x.

Finally enable the module.

## Configuration

+ Create a ````integer```` field for your users to store the Drupal.org user ID.
+ Create a ````plain text```` field for your users to store the GitHub.com user name.
+ Go on /admin/config/people/developers_activity, select the fields and save.

## Use

+ Edit one or more users and fill-in the Drupal.org & GitHub fields.
+ At each cron, their feeds will be imported as *developer_activity_item* entities. You can manage them on /admin/content/developer_activity_item, for instance to unpublish some of them.
+ You can create Views of *Developer activity item* entity.
+ The migrations can be seen on /admin/structure/migrate/manage/developer_activity/migrations. However you can't launch them manually because they require some parameters that are passed when running through hook_cron().
+ If you have [Migrate tools](https://www.drupal.org/project/migrate_tools), you can rollback individual migrations if you want:

````
drush migrate:rollback developer_activity_github
drush migrate:rollback developer_activity_drupal_node
drush migrate:rollback developer_activity_drupal_comment
````

Or all of them: ````drush migrate:rollback --group developer_activity````
