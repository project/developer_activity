<?php

/**
 * @file
 * Contains developer_activity.module.
 */

use Drupal\Core\Url;
use Drupal\user\Entity\User;

const DEVELOPER_ACTIVITY_ENDPOINT_DRUPAL_NODE = 'https://www.drupal.org/api-d7/node.json';
const DEVELOPER_ACTIVITY_ENDPOINT_DRUPAL_COMMENT = 'https://www.drupal.org/api-d7/comment.json';
const DEVELOPER_ACTIVITY_ENDPOINT_GITHUB = 'https://github.com/';

/**
 * Implements hook_cron().
 */
function developer_activity_cron() {
  $config = \Drupal::config('developer_activity.settings');
  $drupalField = $config->get('drupal_user_id_field');
  $githubField = $config->get('github_user_name_field');
  if (
    ($drupalField && $drupalField !== '') ||
    ($githubField && $githubField !== '')
  ) {
    $users = User::loadMultiple();
    foreach ($users as $user) {
      // Fetch Drupal.org.
      if ($drupalUserId = $user->{$drupalField}->value) {
        // Nodes.
        $url = Url::fromUri(DEVELOPER_ACTIVITY_ENDPOINT_DRUPAL_NODE, [
          'query' => [
            'sort' => 'nid',
            'direction' => 'DESC',
            'author' => $drupalUserId,
          ],
        ])->toString();
        $queue = \Drupal::queue('developer_activity');
        $data = [
          'type' => 'drupal_node',
          'url' => $url,
          'user_id' => $user->id(),
        ];
        $queue->createItem($data);
        // Comments.
        $url = Url::fromUri(DEVELOPER_ACTIVITY_ENDPOINT_DRUPAL_COMMENT, [
          'query' => [
            'sort' => 'cid',
            'direction' => 'DESC',
            'author' => $drupalUserId,
          ],
        ])->toString();
        $queue = \Drupal::queue('developer_activity');
        $data = [
          'type' => 'drupal_comment',
          'url' => $url,
          'user_id' => $user->id(),
        ];
        $queue->createItem($data);
      }
      // Fetch GitHub.
      if ($githubUserName = $user->{$githubField}->value) {
        $url = DEVELOPER_ACTIVITY_ENDPOINT_GITHUB . $githubUserName . '.atom';
        $queue = \Drupal::queue('developer_activity');
        $data = [
          'type' => 'github',
          'url' => $url,
          'user_id' => $user->id(),
        ];
        $queue->createItem($data);
      }
    }
  }
}
