<?php

namespace Drupal\developer_activity\Plugin\QueueWorker;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\MigrateMessage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the developer_activity queueworker.
 *
 * @QueueWorker (
 *   id = "developer_activity",
 *   title = @Translation("Fetches Developer activity items."),
 *   cron = {"time" = 30}
 * )
 */
class DeveloperActivityQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The migration manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $migrationManager;

  /**
   * Construct.
   */
  public function __construct(PluginManagerInterface $migration_manager) {
    $this->migrationManager = $migration_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('plugin.manager.migration')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    switch ($data['type']) {
      case 'drupal_node':
        $migrationId = 'developer_activity_drupal_node';
        break;

      case 'drupal_comment':
        $migrationId = 'developer_activity_drupal_comment';
        break;

      case 'github':
        $migrationId = 'developer_activity_github';
        break;
    }
    $migration = $this->migrationManager->createInstance($migrationId, [
      'source' => [
        'urls' => [$data['url']],
      ],
      'process' => [
        'user_id' => [
          'default_value' => $data['user_id'],
        ],
      ],
    ]);
    $executable = new MigrateExecutable($migration, new MigrateMessage());
    $executable->import();
  }

}
