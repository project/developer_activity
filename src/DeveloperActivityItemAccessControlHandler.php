<?php

namespace Drupal\developer_activity;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Developer activity item entity.
 *
 * @see \Drupal\developer_activity\Entity\DeveloperActivityItem.
 */
class DeveloperActivityItemAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\developer_activity\Entity\DeveloperActivityItemInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished developer activity item entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published developer activity item entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit developer activity item entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete developer activity item entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add developer activity item entities');
  }

}
