<?php

namespace Drupal\developer_activity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Developer activity item entities.
 *
 * @ingroup developer_activity
 */
class DeveloperActivityItemListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['user_id'] = $this->t('Owner');
    $header['title'] = $this->t('Title');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\developer_activity\Entity\DeveloperActivityItem $entity */
    $row['id'] = $entity->id();
    $row['user_id'] = Link::createFromRoute(
      $entity->getOwner()->getDisplayName(),
      'entity.user.canonical',
      ['user' => $entity->getOwnerId()]
    );

    $row['title'] = Link::createFromRoute(
      $entity->link->title,
      'entity.developer_activity_item.edit_form',
      ['developer_activity_item' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
