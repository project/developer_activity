<?php

namespace Drupal\developer_activity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Developer activity item entities.
 *
 * @ingroup developer_activity
 */
class DeveloperActivityItemDeleteForm extends ContentEntityDeleteForm {


}
