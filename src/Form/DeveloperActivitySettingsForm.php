<?php

namespace Drupal\developer_activity\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Developer activity settings for this site.
 */
class DeveloperActivitySettingsForm extends ConfigFormBase {

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Construct.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager) {
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager'),
    );
  }

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'developer_activity.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'developer_activity_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    // Select field for Drupal user ID.
    $drupalFieldOptions = [];
    $allIntegerFields = $this->entityFieldManager->getFieldMapByFieldType('integer');
    if (isset($allIntegerFields['user'])) {
      foreach ($allIntegerFields['user'] as $fieldName => $details) {
        $drupalFieldOptions[$fieldName] = $fieldName;
      }
    }
    $form['drupal_user_id_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal user ID field'),
      '#description' => $this->t('Choose an integer field storing the Drupal user ID.'),
      '#options' => $drupalFieldOptions,
      '#default_value' => $config->get('drupal_user_id_field') ?? '',
    ];

    // Select field for GitHub user name.
    $githubFieldOptions = [];
    $allStringFields = $this->entityFieldManager->getFieldMapByFieldType('string');
    if (isset($allStringFields['user'])) {
      foreach ($allStringFields['user'] as $fieldName => $details) {
        $githubFieldOptions[$fieldName] = $fieldName;
      }
    }
    $form['github_user_name_field'] = [
      '#type' => 'select',
      '#title' => $this->t('GitHub user name field'),
      '#description' => $this->t('Choose a plain text field storing the GitHub user name.'),
      '#options' => $githubFieldOptions,
      '#default_value' => $config->get('github_user_name_field') ?? '',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('drupal_user_id_field', $form_state->getValue('drupal_user_id_field'))
      ->set('github_user_name_field', $form_state->getValue('github_user_name_field'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
