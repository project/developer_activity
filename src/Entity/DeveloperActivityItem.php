<?php

namespace Drupal\developer_activity\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\link\LinkItemInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Developer activity item entity.
 *
 * @ingroup developer_activity
 *
 * @ContentEntityType(
 *   id = "developer_activity_item",
 *   label = @Translation("Developer activity item"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\developer_activity\DeveloperActivityItemListBuilder",
 *     "views_data" = "Drupal\developer_activity\Entity\DeveloperActivityItemViewsData",
 *     "form" = {
 *       "default" = "Drupal\developer_activity\Form\DeveloperActivityItemForm",
 *       "add" = "Drupal\developer_activity\Form\DeveloperActivityItemForm",
 *       "edit" = "Drupal\developer_activity\Form\DeveloperActivityItemForm",
 *       "delete" = "Drupal\developer_activity\Form\DeveloperActivityItemDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\developer_activity\DeveloperActivityItemHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\developer_activity\DeveloperActivityItemAccessControlHandler",
 *   },
 *   base_table = "developer_activity_item",
 *   translatable = FALSE,
 *   admin_permission = "administer developer activity item entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/developer_activity_item/{developer_activity_item}",
 *     "add-form" = "/admin/content/developer_activity_item/add",
 *     "edit-form" = "/admin/content/developer_activity_item/{developer_activity_item}/edit",
 *     "delete-form" = "/admin/content/developer_activity_item/{developer_activity_item}/delete",
 *     "collection" = "/admin/content/developer_activity_item",
 *   },
 *   field_ui_base_route = "developer_activity_item.settings"
 * )
 */
class DeveloperActivityItem extends ContentEntityBase implements DeveloperActivityItemInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Link (URL + title).
    $fields['link'] = BaseFieldDefinition::create('link')
      ->setLabel(t('Link'))
      ->setRequired(TRUE)
      ->setSettings([
        'link_type' => LinkItemInterface::LINK_GENERIC,
        'title' => DRUPAL_REQUIRED,
        'options' => [
          'attributes' => [
            'target' => '_blank',
            'rel' => 'nofollow',
          ],
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'link',
        'weight' => 1,
      ])
      ->setDisplayOptions('view', [
        'type' => 'link',
        'weight' => 0,
        'label' => 'above',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Date of the original publication.
    $fields['published'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Published at'))
      ->setDescription(t('The time that the remote item was originally published.'))
      ->setRequired(TRUE)
      ->setSettings([
        'datetime_type' => 'datetime',
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('view', TRUE);

    // Owner.
    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Owner'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 3,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Status.
    $fields += static::publishedBaseFieldDefinitions($entity_type);
    $fields['status']
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 4,
      ]);

    // Created.
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'));

    // Changed.
    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'));

    return $fields;
  }

}
