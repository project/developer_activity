<?php

namespace Drupal\developer_activity\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Developer activity item entities.
 *
 * @ingroup developer_activity
 */
interface DeveloperActivityItemInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Gets the Developer activity item creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Developer activity item.
   */
  public function getCreatedTime();

  /**
   * Sets the Developer activity item creation timestamp.
   *
   * @param int $timestamp
   *   The Developer activity item creation timestamp.
   *
   * @return \Drupal\developer_activity\Entity\DeveloperActivityItemInterface
   *   The called Developer activity item entity.
   */
  public function setCreatedTime($timestamp);

}
