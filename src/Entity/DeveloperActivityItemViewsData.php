<?php

namespace Drupal\developer_activity\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Developer activity item entities.
 */
class DeveloperActivityItemViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();
    return $data;
  }

}
